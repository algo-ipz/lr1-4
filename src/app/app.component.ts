import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'my-app';
  posts: Post[] = [
    {
      title: 'React',
      text: 'JavaScript-библиотека для создания пользовательских интерфейсов',
      id: 1,
    },
    {
      title: 'Angular',
      text: 'Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps.',
      id: 2,
    },
    {
      title: 'Vue',
      text: 'Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces',
      id: 3,
    },
    {
      title: 'Node.js',
      text: 'Node.js® — це JavaScript–оточення побудоване на JavaScript–рушієві Chrome V8.',
      id: 4,
    },
  ];

  updatePosts(event: Post) {
    this.posts.unshift({ ...event, id: 0 });
    this.posts.forEach((post, index) => {
      post.id = index + 1;
    });
  }
  removeIdPost(id: number) {
    this.posts = this.posts.filter((item) => item.id != id);
  }

  titleS = '';
  onChangeSearch(event: string) {
    this.titleS = event;
  }

  searchByType = SearchType.BY_TITLE
  onSearchByType(event: SearchType) {
    this.searchByType = event;
  }
}

export interface Post {
  title: string;
  text: string;
  id?: number;
}

export enum SearchType {
  BY_TITLE = 'title',
  BY_CONTENT = 'content'
}
