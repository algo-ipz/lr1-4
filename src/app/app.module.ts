import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PostFormComponent } from './post-form/post-form.component';
import { PostComponent } from './post/post.component';
import { IpzDirective } from './directives/ipz.directive';
import { AlertDirective } from './directives/alert.directive copy';
import { FilterPipe } from './pipes/filter.pipe';

@NgModule({
  declarations: [AppComponent, PostFormComponent, PostComponent, IpzDirective, AlertDirective, FilterPipe],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
