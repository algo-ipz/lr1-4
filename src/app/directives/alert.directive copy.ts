import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appAlert]',
})
export class AlertDirective {
  @Input('appAlert') postName!: string;

  @HostListener('click') onClick() {
    alert(this.postName);
  }
}
