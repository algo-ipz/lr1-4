import { Pipe, PipeTransform } from '@angular/core';
import { Post, SearchType } from '../app.component';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(post: Post[], titleSearch: string, searchByType: SearchType): Post[] {
    if (!titleSearch.trim()) {
      return post;
    } else {
      switch (searchByType) {
        case SearchType.BY_TITLE:
          return post.filter((item) => {
            return item.title.toLowerCase().includes(titleSearch.toLowerCase());
          });
        case SearchType.BY_CONTENT:
          return post.filter((item) => {
            return item.text.toLowerCase().includes(titleSearch.toLowerCase());
          });
        default:
          return post;
      }
    }
  }
}
